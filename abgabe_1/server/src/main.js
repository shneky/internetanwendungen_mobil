const express = require('express')

const app = express()
const port = 8394

app.use(express.static('../client'))
app.use('/data', express.static('data'))

app.listen(port, () => console.log(`app listening on port ${port}!`))