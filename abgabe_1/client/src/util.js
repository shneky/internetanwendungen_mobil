const fadeOut = (elem, callback) => {
  let step = 0.01
  let intervalTime = step * 2000

  let opacity = 1
  elem.style.opacity = opacity
  let interval = setInterval(() => {
    if (opacity > 0) {
      opacity -= step
      elem.style.opacity = opacity
    } else {
      clearInterval(interval)
      callback()
    }
  }, intervalTime)
}

const fadeIn = (elem, callback) => {
  let step = 0.01
  let intervalTime = step * 1000

  let opacity = 0
  elem.style.opacity = opacity
  let interval = setInterval(() => {
    if (opacity < 1) {
      opacity += step
      elem.style.opacity = opacity
    } else {
      clearInterval(interval)
      callback()
    }
  }, intervalTime)
}