const getUl = () => {
  const content = document.getElementById('content')
  return content.getElementsByTagName('ul')[0]
}

const clearUl = () => {
  const ul = getUl()
  while (ul.firstChild) {
    ul.removeChild(ul.firstChild)
  }
}

const getListItems = (callback) => {
  fetch('/data/listitems.json').then(res => {
    return res.json()
  }).then(items => {

    clearUl()

    for (item of items) {
      addListItem(item)
    }
    callback()
  });
}

const addListItem = (item) => {
  const ul = getUl()

  const li = document.createElement('li')

  const changeViewBtn = document.getElementById('changeViewBtn')
  const icons = changeViewBtn.getElementsByTagName('i')

  if (icons[0].className.includes('fa-th')) {
    li.className = 'track-li'
  } else {
    li.className = 'tile-li'
  }

  li.innerHTML = /*html*/`
    <div class="track-text">
      <div class="track-left">
        <img src="${item.src}">
        <div class="track-left-text">
          <p class="track-band text-gray">${item.owner}</p>
          <p class="track-title">${item.name}</p>
          <p class="track-progress text-gray">
            <i class="fas fa-play"></i>
            ${item.numOfTags}
          </p>
        </div>
      </div>
      <div class="track-right">
        <p class="track-date text-gray">${item.added}</p>
        <span class="btn track-menu text-gray">
          <i class="fas fa-ellipsis-v"></i>
        </span>
      </div>
    </div>
  `.trim()
  ul.appendChild(li)
}



const addOnClickToListItems = () => {
  const content = document.getElementById('content')
  const ul = getUl()
  for (const li of content.getElementsByTagName('li')) {
    li.onclick = (event) => {
      const title = li.getElementsByClassName('track-title')[0].textContent

      //track-menu is pressed
      if (event.target.parentNode.className.includes('track-menu')) {
        const url = li.getElementsByTagName('img')[0].getAttribute('src')
        const msg = 'Wollen sie diesen Titel entfernen?'
        const shouldRemove = 
          confirm (`Titel: ${title}\nURL: ${url}\n${msg}`)
        if (shouldRemove) {
          ul.removeChild(li)
        }
      } else {
        alert(`Titel: ${title}`)
      }
    }
  }
}